#include <iostream>
#include <random>
#include <pcl/point_types.h>
#include <pcl/filters/crop_box.h>
#include <pcl/visualization/cloud_viewer.h>

/**
 * Example demonstrating usage of pcl::CropBox
 */
int main(int argc, char **argv) {

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>());

  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(0.0,5.0);

  for (int i = 0; i < 10000; ++i) {
    pcl::PointXYZRGB point(255, 0, 0);
    point.x = distribution(generator);
    point.y = distribution(generator);
    point.z = distribution(generator);
    cloud->push_back(point);
  }

  //pcl::PointXYZRGB point(255, 0, 0);
  //point.x = 0;
  //point.y = 0;
  //point.z = 0;

  //cloud->push_back(point);

  //point.x = 1;
  //cloud->push_back(point);

  //point.x = 2;
  //cloud->push_back(point);

  //point.x = 3;
  //cloud->push_back(point);

  pcl::CropBox<pcl::PointXYZRGB> cropbox;

  double minX = -1.0;
  double minY = -1.0;
  double minZ = -1.0;

  double maxX = 1.0;
  double maxY = 1.0;
  double maxZ = 1.0;
  cropbox.setMin(Eigen::Vector4f(minX, minY, minZ, 1.0));
  cropbox.setMax(Eigen::Vector4f(maxX, maxY, maxZ, 1.0));

  Eigen::Vector3f box_translation(0, 0, 0);
  cropbox.setTranslation(box_translation);

  Eigen::Vector3f box_roll_pitch_yaw(0, 0, 0);
  cropbox.setRotation(box_roll_pitch_yaw);

  
  cropbox.setInputCloud(cloud);

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr filtered_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
  cropbox.filter(*filtered_cloud);

  // Change the filtered color to green.
  for (pcl::PointXYZRGB& point_ref : filtered_cloud->points) {
      point_ref.r = 0;
      point_ref.g = 255;
  }

  pcl::visualization::PCLVisualizer::Ptr viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));

  viewer->addPointCloud<pcl::PointXYZRGB>(cloud, "before filter");
  viewer->addPointCloud<pcl::PointXYZRGB>(filtered_cloud, "after filter");

  Eigen::Affine3f trans = pcl::getTransformation(0, 0, 0, box_roll_pitch_yaw[0], box_roll_pitch_yaw[1], box_roll_pitch_yaw[2]);

  Eigen::Quaternion<float> quat(trans.rotation());

  double depth = maxX - minX;
  double width = maxY - minY;
  double height = maxZ - minZ;
  
  viewer->addCube(box_translation, quat, width, height, depth);
  viewer->setRepresentationToWireframeForAllActors();
  while (!viewer->wasStopped ())
  {
    viewer->spinOnce (100);
    boost::this_thread::sleep (boost::posix_time::microseconds (100000));
  }


  return (0);
}
