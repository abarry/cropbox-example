# CropBox Example
A simple example demonstrating how to use pcl::CropBox.

To build:
```
mkdir build
cd build
cmake ..
make
./cropbox_example
```


![Example output](example.jpg)
